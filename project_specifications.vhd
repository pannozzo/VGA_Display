library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.fixed_pkg.all;

package project_specifications is
    



	--https://timetoexplore.net/blog/arty-fpga-vga-verilog-01

	--http://tinyvga.com/vga-timing/1280x1024@60Hz
    


	--type VGA_1280x1024_Timing is record
	--	constant CLOCK_SPEED 					: natural := 108_000_000;
	--    constant FRAME_WIDTH 					: natural := 1280;
	--    constant FRAME_HEIGHT 					: natural := 1024;
	--    constant HORIZONTAL_FRONT_PORCH 		: natural := 48; --H front porch width (pixels)
	--    constant HORIZONTAL_SYNC_PULSE_WIDTH 	: natural := 112; --H sync pulse width (pixels)
	--    constant HORIZONTAL_SYNC_PULSE_START 	: natural := HORIZONTAL_FRONT_PORCH + FRAME_WIDTH - 1;
	--    constant HORIZONTAL_SYNC_PULSE_END		: natural := HORIZONTAL_FRONT_PORCH + FRAME_WIDTH + HORIZONTAL_SYNC_PULSE_WIDTH - 1;
	--    constant HORIZONTAL_WHOLE_LINE 			: natural := 1688; --H total period (pixels)
	--    constant VERTICAL_FRONT_PORCH 			: natural := 1; --V front porch width (lines)
	--    constant VERTICAL_SYNC_PULSE_WIDTH 		: natural := 3; --V sync pulse width (lines)
	--    constant VERTICAL_SYNC_PULSE_START 		: natural := VERTICAL_FRONT_PORCH + FRAME_HEIGHT - 1;
	--    constant VERTICAL_SYNC_PULSE_END 		: natural := VERTICAL_FRONT_PORCH + FRAME_HEIGHT + VERTICAL_SYNC_PULSE_WIDTH - 1;
	--    constant VERTICAL_WHOLE_LINE 			: natural := 1066; --V total period (lines)
	--end record VGA_1280x1024_Timing;




	constant VGA1280x1024_pixel_clock_speed : natural := 108_000_000;
    constant FRAME_WIDTH 					: natural := 1280;
    constant FRAME_HEIGHT 					: natural := 1024;
    constant HORIZONTAL_FRONT_PORCH 		: natural := 48; --H front porch width (pixels)
    constant HORIZONTAL_SYNC_PULSE_WIDTH 	: natural := 112; --H sync pulse width (pixels)
    constant HORIZONTAL_SYNC_PULSE_START 	: natural := HORIZONTAL_FRONT_PORCH + FRAME_WIDTH - 1;
    constant HORIZONTAL_SYNC_PULSE_END		: natural := HORIZONTAL_FRONT_PORCH + FRAME_WIDTH + HORIZONTAL_SYNC_PULSE_WIDTH - 1;
    constant HORIZONTAL_WHOLE_LINE 			: natural := 1688; --H total period (pixels)
    constant VERTICAL_FRONT_PORCH 			: natural := 1; --V front porch width (lines)
    constant VERTICAL_SYNC_PULSE_WIDTH 		: natural := 3; --V sync pulse width (lines)
    constant VERTICAL_SYNC_PULSE_START 		: natural := VERTICAL_FRONT_PORCH + FRAME_HEIGHT - 1;
    constant VERTICAL_SYNC_PULSE_END 		: natural := VERTICAL_FRONT_PORCH + FRAME_HEIGHT + VERTICAL_SYNC_PULSE_WIDTH - 1;
    constant VERTICAL_WHOLE_LINE 			: natural := 1066; --V total period (lines)
    

--    constant HALVED_FRAME_WIDTH : natural   := FRAME_WIDTH / 2;
--    constant HALVED_FRAME_HEIGHT : natural  := FRAME_HEIGHT / 2;
--    constant FOUR_DIV_FRAME_WIDTH : natural := 4 / FRAME_WIDTH;
    
    
    constant HALVED_FRAME_WIDTH : sfixed := to_sfixed(FRAME_WIDTH, 11, 0) / 2;
    constant HALVED_FRAME_HEIGHT : sfixed := to_sfixed(FRAME_HEIGHT, 11, 0) / 2;
    constant FOUR_DIV_FRAME_WIDTH : sfixed := to_sfixed(4, 1, -10) / FRAME_WIDTH;


    --I don't actually know what these do?
    constant H_POL : std_logic := '1';
    constant V_POL : std_logic := '1';
    
    
    
--	constant VGA640x480_pixel_clock 	: natural := 25_000_000; 


--	constant horizontal_active_pixels 	: natural := 640;
--    constant horizontal_front_porch 	: natural := 16;
--    constant horizontal_sync_width 		: natural := 96;
--    constant horizontal_back_porch 		: natural := 48;
--    constant horizontal_total_pixels 	: natural := 800; --Sum active pixels, front/back porch, sync width
--    constant horizontal_blanking_total 	: natural := 160;

--    constant horizontal_sync_start		: natural := horizontal_front_porch;
--    constant horizontal_sync_end		: natural := horizontal_sync_start + horizontal_sync_width;
    
--	constant vertical_active_pixels 	: natural := 480;
--	constant vertical_front_porch 		: natural := 11; --Book says 10
--	constant vertical_sync_width 		: natural := 2;
--	constant vertical_back_porch 		: natural := 31; --Book says 33
--	constant vertical_total_pixels 		: natural := 524; --Sum active pixels, front/back porch, sync width
--														  --Book says 525
--	constant vertical_blanking_total 	: natural := 44;

--    constant vertical_sync_start		: natural := vertical_active_pixels + vertical_front_porch;
--    constant vertical_sync_end			: natural := vertical_sync_start + vertical_sync_width;
    

end package project_specifications;