library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.std_logic_unsigned.all;
use ieee.math_real.all;

use work.project_specifications.all;


entity frame_counter is

--Make generic later
	--generic (
	--	HORIZONTAL_MAX_FRAME 	: natural := 1280;
	--	VERTICAL_MAX_FRAME 		: natural := 1024
	--);

	port (
		clock_108MHz       	: in  std_logic;
		reset				: in  std_logic;
		increment			: in  std_logic;
		sync_clear			: in  std_logic;

	  -- Horizontal and Vertical counters
        horizontal_count 	: out std_logic_vector(11 downto 0) := (others =>'0');
        vertical_count 		: out std_logic_vector(11 downto 0) := (others =>'0');
        
    	-- Horizontal and Vertical Sync
        horizontal_sync 	: out std_logic := not(H_POL);
        vertical_sync 		: out std_logic := not(V_POL)
	);

end frame_counter;

architecture textbook of frame_counter is 

begin

	process (clock_108MHz)
	begin
		if (rising_edge(clock_108MHz)) then
			if (horizontal_count = (HORIZONTAL_WHOLE_LINE - 1)) then
		   		horizontal_count <= (others => '0');
		 	else
		   		horizontal_count <= horizontal_count + 1;
		 	end if;
		end if;
	end process;


	-- Vertical counter
	process (clock_108MHz)
	begin 
		if (rising_edge(clock_108MHz)) then
			if ((horizontal_count = (HORIZONTAL_WHOLE_LINE - 1)) and (vertical_count = (VERTICAL_WHOLE_LINE - 1))) then
				vertical_count <= (others => '0');
			elsif (horizontal_count = (HORIZONTAL_WHOLE_LINE - 1)) then
				vertical_count <= vertical_count + 1;
			end if;
		end if;
	end process;


	-- Horizontal sync
	process (clock_108MHz)
	begin
		if (rising_edge(clock_108MHz)) then
			if (horizontal_count >= (HORIZONTAL_SYNC_PULSE_START)) and (horizontal_count < (HORIZONTAL_SYNC_PULSE_END)) then
				horizontal_sync <= H_POL;
			else
				horizontal_sync <= not(H_POL);
			end if;
		end if;
	end process;


	-- Vertical sync
	process (clock_108MHz)
	begin
		if (rising_edge(clock_108MHz)) then
			if (vertical_count >= VERTICAL_SYNC_PULSE_START) and (vertical_count < VERTICAL_SYNC_PULSE_END) then
				vertical_sync <= V_POL;
			else
				vertical_sync <= not(V_POL);
			end if;
		end if;
	end process;

end architecture;
	




